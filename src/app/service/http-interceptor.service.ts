import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    req = req.clone({
      setHeaders: {
        Authorization: 'Bearer xxxxxxxxxxxxxxxxxxxx'
      }
    });

    return next.handle(req)
      .pipe(
        tap(evt => {
          if (evt instanceof HttpResponse) {
            if (evt.status.toString().startsWith("2")) {
              console.log('oke =>', evt.body.data);
            }
          }
        }),
        catchError(err => {
          console.log('error =>', JSON.stringify(err));
          return of(err)
        }))
  }
}

